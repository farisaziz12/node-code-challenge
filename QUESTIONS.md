# Questions

Qs1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```

The code will first console log the string "2" and then after the set timout of 100ms the string "1" will be console logged. This is because when Javascript 
encounters the setTimout function it schedules the callback function that logs 1 to be excecuted after the specified delay. Meanwhile, excecution continues immediately to the following statement that logs 2.

Qs2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```

The function foo will log the integers from 10 to 0. This is because it calls itself recursively as long as the argument d is less than 10. On each call to itself d is incremente by 1. When d reaches a value of 10 the recursive call is skipped and d having a value of 10 is logged. Then each call from the function foo returns and logs the d in its scope, which goes from 9 to 0

Qs3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```

The potential issue with the code is that if a falsey value such as 0 is given as an argument the result of the console log would be 5. To avoid this one
could use a ternary operator to check if d is falsey or not such as "d == null? 5 : d"

Qs4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```
The output of the following code will be the integer 3. This is because the argument "a" is given the integer 1 for the function  "foo"
and then when the result of the function "foo" is assigned to the variable bar it is  console logged with the argument of integer 2 that is passed on to
the nested function within the function "foo" that returns the sum of the arguments within scope, which are a and b.

Qs5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```
The above function would be used to take in an integer and function "done" as arguments and double the integer's value and pass
it in as an argument to a the provided "done". It can be used to console log the doubled integer like this: double(5, console.log)