const search = document.querySelector("#search")
const results = document.querySelector("#results")

search.addEventListener('change',  () => {
    if (search.value.length > 2) {
        fetch(`http://localhost:3000/locations?q=${search.value}`, {
        method: 'POST', 
        headers: 
        {'Content-Type': 'application/json'}
        }).then(resp =>  resp.json()).then(locations => Sortsearch(locations))
    }
})

String.prototype.match = function(term) {
    if (term.length === 0)
        return 1;
    let string = this.toLowerCase();
    let compare = term.toLowerCase();
    let matches = 0;
    for (let i = 0; i < compare.length; i++) {
        string.indexOf(compare[i]) > -1 ? matches += 1 : matches -=1;
    }
    return matches / this.length;
};

function Sortsearch(locations) {
    const results = locations.data.map(location => [location, location.name.match(search.value)]);
    results.sort((a, b) => b[1] - a[1]);
    renderResults(results);
}


const renderResults = locations => {
    const oldDiv = document.querySelector("#div")
    if(oldDiv) oldDiv.remove()
    const newDiv = document.createElement("div")
    newDiv.id = "div"
    locations.map(location => {
        const h3 = document.createElement("h3")
        h3.textContent = `* ${location[0].name} (Latitude: ${location[0].latitude}, Longitude: ${location[0].longitude})`
        newDiv.append(h3)
        results.append(newDiv)
    })
}